# GitHubDetails
Get some details of GitHub repository. Project made in 2019.

## Usage
Build project: 

`mvn clean install`

Change directory to the one containing jar file and run:

`java -jar github-details.jar`

## API
GET /repositories/{owner}/{repository-name}
