package pl.cd.githubDetails;

/**
 * Create JSON, which mocks 404 response from external service
 */
public final class GitHubApiNotFoundResponse {

	private final String message;

	private final String documentation_url;

	public GitHubApiNotFoundResponse(String message, String documentation_url) {
		super();
		this.message = message;
		this.documentation_url = documentation_url;
	}

	public String getMessage() {
		return message;
	}

	public String getDocumentation_url() {
		return documentation_url;
	}

}
