package pl.cd.githubDetails;

/**
 * Create JSON, which mocks 200 response from external service
 */
public final class GitHubApiOKResponse {

	private final Long id;

	private final String node_id;

	private final String full_name;

	private final String description;

	private final String clone_url;

	private final Long stargazers_count;

	private final String created_at;

	private final Long watchers_count;

	private final Boolean fork;

	public GitHubApiOKResponse(Long id, String node_id, String full_name, String description, String clone_url,
			Long stargazers_count, String created_at, Long watchers_count, Boolean fork) {
		super();
		this.id = id;
		this.node_id = node_id;
		this.full_name = full_name;
		this.description = description;
		this.clone_url = clone_url;
		this.stargazers_count = stargazers_count;
		this.created_at = created_at;
		this.watchers_count = watchers_count;
		this.fork = fork;
	}

	public String getFull_name() {
		return full_name;
	}

	public String getDescription() {
		return description;
	}

	public String getClone_url() {
		return clone_url;
	}

	public Long getStargazers_count() {
		return stargazers_count;
	}

	public String getCreated_at() {
		return created_at;
	}

	public Long getId() {
		return id;
	}

	public String getNode_id() {
		return node_id;
	}

	public Long getWatchers_count() {
		return watchers_count;
	}

	public Boolean getFork() {
		return fork;
	}

}
