package pl.cd.githubDetails;

import static org.junit.Assert.assertTrue;
import static org.springframework.test.web.client.match.MockRestRequestMatchers.method;
import static org.springframework.test.web.client.match.MockRestRequestMatchers.requestTo;
import static org.springframework.test.web.client.response.MockRestResponseCreators.withServerError;
import static org.springframework.test.web.client.response.MockRestResponseCreators.withStatus;
import static org.springframework.test.web.client.response.MockRestResponseCreators.withSuccess;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpMethod;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.web.client.MockRestServiceServer;
import org.springframework.web.client.RestTemplate;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;

import pl.cd.githubdetails.GitController;
import pl.cd.githubdetails.GitResponse;

/**
 * There are made 4 requests during tests with use of mock server, to check the application behaviour
 * in case of receiving specific response codes from external service
 */
@RunWith(SpringRunner.class)
@ContextConfiguration(locations = { "classpath:/applicationContext.xml" })
public class GitHubDetailsApplicationTests {

	@Autowired
	private RestTemplate restTemplate;

	private MockRestServiceServer mockServer;

	@Autowired
	private GitController gitController;

	@Autowired
	private ObjectMapper mapper;

	@Before
	public void setUp() {
		this.mockServer = MockRestServiceServer.createServer(restTemplate);
	}

	@Test
	public void test200() {

		GitHubApiOKResponse mockResponse = new GitHubApiOKResponse(441854l, "MDEwOlJlcG9zaXRvcnk0NDE4NTQ=",
				"evilstreak/markdown-js", "A Markdown parser for javascript",
				"https://github.com/evilstreak/markdown-js.git", 7250l, "2009-12-18T15:31:48Z", 7250l, Boolean.FALSE);

		String jsonString = null;
		try {
			jsonString = mapper.writeValueAsString(mockResponse);
		} catch (JsonProcessingException e) {
			e.printStackTrace();
		}
		mockServer.expect(requestTo("https://api.github.com/repos/evilstreak/markdown-js"))
				.andExpect(method(HttpMethod.GET)).andRespond(withSuccess(jsonString, MediaType.APPLICATION_JSON));

		GitResponse result = gitController.getRepoDetails("evilstreak", "markdown-js");

		mockServer.verify();
		assertTrue(result.getStars() != null && result.getStars() >= 0l);
		assertTrue(result.getCreatedAt() != null && TestHelper.checkIfDateIsIsoFormat(result.getCreatedAt()));
		assertTrue(result.getFullName() != null && !result.getFullName().isEmpty());
		assertTrue(result.getCloneUrl() != null && TestHelper.checkIfUrlIsCorrect(result.getCloneUrl()));
	}

	@Test
	public void test404() {

		GitHubApiNotFoundResponse mockResponse = new GitHubApiNotFoundResponse("Not Found",
				"https://developer.github.com/v3/repos/#get");

		String jsonString = null;
		try {
			jsonString = mapper.writeValueAsString(mockResponse);
		} catch (JsonProcessingException e) {
			e.printStackTrace();
		}
		mockServer.expect(requestTo("https://api.github.com/repos/evilstreakAABBAABB/markdown-js"))
				.andExpect(method(HttpMethod.GET)).andRespond(withStatus(HttpStatus.NOT_FOUND).body(jsonString));

		String result = gitController.getRepoDetails("evilstreakAABBAABB", "markdown-js");

		mockServer.verify();
		assertTrue(result.startsWith("Error: 404 NOT_FOUND"));
	}

	@Test
	public void test500() {
		mockServer.expect(requestTo("https://api.github.com/repos/evilstreak/markdown-js"))
				.andExpect(method(HttpMethod.GET)).andRespond(withServerError());

		String result = gitController.getRepoDetails("evilstreak", "markdown-js");

		mockServer.verify();
		assertTrue(result.startsWith("Error: 500 INTERNAL_SERVER_ERROR"));
	}

	@Test
	public void test503() {
		mockServer.expect(requestTo("https://api.github.com/repos/evilstreak/markdown-js"))
				.andExpect(method(HttpMethod.GET)).andRespond(withStatus(HttpStatus.SERVICE_UNAVAILABLE));

		String result = gitController.getRepoDetails("evilstreak", "markdown-js");

		mockServer.verify();
		assertTrue(result.startsWith("Error: 503 SERVICE_UNAVAILABLE"));
	}

}
