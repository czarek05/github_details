package pl.cd.githubDetails;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.TimeZone;

import org.apache.commons.validator.routines.UrlValidator;

public class TestHelper {

	public static Boolean checkIfDateIsIsoFormat(String strDate) {
		SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss'Z'");
		sdf.setTimeZone(TimeZone.getTimeZone("GMT"));
		try {
			sdf.parse(strDate);
		} catch (ParseException e) {
			return Boolean.FALSE;
		}
		return Boolean.TRUE;

	}

	public static Boolean checkIfUrlIsCorrect(String url) {
		UrlValidator urlValidator = new UrlValidator();
		return urlValidator.isValid(url);
	}
}
