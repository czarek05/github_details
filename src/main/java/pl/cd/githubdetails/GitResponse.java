package pl.cd.githubdetails;

import com.fasterxml.jackson.annotation.JsonAlias;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;

@JsonIgnoreProperties(ignoreUnknown = true)
public final class GitResponse {

	@JsonAlias({ "full_name" })
	private final String fullName;

	private final String description;

	@JsonAlias({ "clone_url" })
	private final String cloneUrl;

	@JsonAlias({ "stargazers_count" })
	private final Long stars;

	@JsonAlias({ "created_at" })
	private final String createdAt;

	public GitResponse(String fullName, String description, String cloneUrl, Long stars, String createdAt) {
		super();
		this.fullName = fullName;
		this.description = description;
		this.cloneUrl = cloneUrl;
		this.stars = stars;
		this.createdAt = createdAt;
	}

	public String getFullName() {
		return fullName;
	}

	public String getDescription() {
		return description;
	}

	public String getCloneUrl() {
		return cloneUrl;
	}

	public Long getStars() {
		return stars;
	}

	public String getCreatedAt() {
		return createdAt;
	}

}
