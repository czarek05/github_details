package pl.cd.githubdetails;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.ParameterizedTypeReference;
import org.springframework.http.HttpMethod;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;
import org.springframework.web.client.HttpClientErrorException;
import org.springframework.web.client.HttpServerErrorException;
import org.springframework.web.client.ResourceAccessException;
import org.springframework.web.client.RestTemplate;

@Service
public class GitHttpClient {

	@Autowired
	private RestTemplate restTemplate;

	public <T> ResponseEntity<T> gitHubDetailsRequest(String owner, String repositoryName) {

		ResponseEntity<T> response = null;
		try {
			// todo: handle unchecked casts, maybe build a String from external api response?
			response = (ResponseEntity<T>) restTemplate.exchange(
					"https://api.github.com/repos/{owner}/{repositoryName}", HttpMethod.GET, null,
					new ParameterizedTypeReference<GitResponse>() {
					}, owner, repositoryName);
		// todo: maybe use @ControllerAdvice for handling error purposes?
		} catch (HttpClientErrorException e) {
			String responseString = "Error: " + e.getStatusCode() + ". ";
			if (HttpStatus.NOT_FOUND.equals(e.getStatusCode())) {
				responseString += "Please consider checking if chosen repository and/or owner exists. Do you have access rights to requested repistory?";
			}
			response = new ResponseEntity<>((T) responseString, e.getStatusCode());
		} catch (ResourceAccessException e) {
			String responseString = "Error: I/O error on GET request. "
					+ "Please consider checking your network connection";
			response = new ResponseEntity<>((T) responseString, HttpStatus.REQUEST_TIMEOUT);
		} catch (HttpServerErrorException e) {
			String responseString = "Error: " + e.getStatusCode() + ". ";
			if (HttpStatus.SERVICE_UNAVAILABLE.equals(e.getStatusCode())) {
				responseString += "Server may be overloaded. Please try again later.";
			}
			response = new ResponseEntity<>((T) responseString, e.getStatusCode());
		} catch (Exception e) {
			response = new ResponseEntity<>((T) e.getMessage(), HttpStatus.BAD_REQUEST);
		}
		return response;
	}

}
