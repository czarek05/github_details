package pl.cd.githubdetails;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RestController;

@RestController
public class GitController {

	@Autowired
	private GitHttpClient gitHttpClient;

	@GetMapping("/repositories/{owner}/{repositoryName}")
	public <T> T getRepoDetails(@PathVariable String owner, @PathVariable String repositoryName) {
		ResponseEntity<GitResponse> result = gitHttpClient.gitHubDetailsRequest(owner, repositoryName);
		return (T) result.getBody();
	}

}